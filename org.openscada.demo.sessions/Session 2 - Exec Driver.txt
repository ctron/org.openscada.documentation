The "Exec driver" is a driver interface module which pulls information from shell scripts or executes shell scripts based on write commands.

An simple setup is located on the Desktop at "/home/demo/Desktop/git/org.openscada.documentation/org.openscada.sample.exec1". Using an "ant" script the server can simply be started from the command line and terminated by pressing Ctrl-C.

The server also binds to TCP port 1202, so be sure that it is free. It might be bound to the testing server that was started in Session 1 inside the OSTC. In order to shut down that testing server you will need to exit OSTC and restart it.

= Starting the exec driver
 - cd to /home/demo/Desktop/git/org.openscada.documentation/org.openscada.sample.exec1
 - Execute: "ant"
 - Open OSTC, Connect to 1202 like in Session #1

If you want you can play around with the configuration located in the "configuration" folder of the sample. The driver has to be restarted in order to load the new configuration. Be sure that you create a valid XML file that validates agains the XML schema. Using an validating XML editor is recommended.
