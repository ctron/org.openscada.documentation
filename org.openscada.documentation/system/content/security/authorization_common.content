<?xml version="1.0" encoding="UTF-8"?>
<content:fragment xmlns:content="urn:openscada:doc:content" xmlns:fragment="urn:openscada:doc:fragment" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<content:p>
At several points the system will check if the user is allowed to perform the requested operation. Without
proper authorization the operation in question will not be performed! In other cases there are ongoing authorization
requests which are like a continuous request. These ongoing evaluations will report to the requestee when the evaluated
state changes. 
</content:p>

<content:p>
Authorization is only performed in OSGi based services and works like the previously described authentication phase.
All authorization modules registered with OSGi will be queried in the priority order. Each service may
accept, reject or abstain for that request. Each request will contain the following information for the service to
evaluate: 
</content:p>

<content:ul>
	<content:item>Object Type</content:item>
	<content:item>Object ID</content:item>
	<content:item>Action</content:item>
</content:ul>

<content:p>
If the authorization fails the request is not performed and an event log entry will be written by the AE system including
the reason of the rejection as reported by the authorization module.</content:p>

<content:p>
The decision logic is defined by the implementation of the authorization module. If no authorization service is found
or all services abstain, the default action is to reject the request.
</content:p>

<content:section content:title="Authorization Modules">

<content:p>
The following bundles are available: 
</content:p>

<content:section content:title="JavaScript authorization">
<content:p>
The java script authorization service processes each request by running a set of rules with a java script code
fragment that evaluates weather the request is accepted or rejected. The set of rules is specific to the setup.
</content:p>

</content:section> <!--  JavaScript -->

</content:section> <!-- Authorization -->

<content:section content:title="Authorization checkpoints">

<content:p>
Each check consists of the following request information:
</content:p>
<content:ul>
	<content:item>Object Type: The type of object the request will act on</content:item>
	<content:item>Object ID: The id of the object the request will act on</content:item>
	<content:item>Operation: The operation the action will perform</content:item>
	<content:item>Additional Information: Additional information based on the type of request</content:item>
</content:ul>

<content:p>
The following operations will be checked for authorization:
</content:p>

<content:section content:title="System logon">
	<content:p>
	When a user logs on first the user itself is authenticated. If this succeeded the system checks if the user has
	the permission to log on. This will never be checked if the user could not be authenticated. This will be checked
	directly after the user was authenticated.
	</content:p>

	<content:plainText><![CDATA[
ObjectType = "SESSION"
ObjectID = The name of the user
Operation = "CONNECT"
Additional information: <none>]]>
	</content:plainText>
</content:section>

<content:section content:title="Impersonate user">
	<content:p>
	This will be checked when impersonation is requested during other authorization requests.
	</content:p>
	<content:plainText><![CDATA[
ObjectType = "SESSION"
ObjectID = The name of the target user
Operation = "PROXY_USER"
Additional information: <none>]]>
	</content:plainText>
</content:section>

<content:section content:title="Acknowledge Alarm">
	<content:p>
	Checked when requesting an aknowledgement of a monitor. 
	</content:p>
	<content:p>
	This check allows impersonating users. If the impersonation fails, the whole request fails.
	</content:p>
	<content:plainText><![CDATA[
ObjectType = "MONITOR"
ObjectID = The name of the monitor to acknowledge
Operation = "AKN"
Additional information: <none>]]>
	</content:plainText>
</content:section>

<content:section content:title="Write value to item">
	<content:p>
	Checked when requesting to write a value to an item. 
	</content:p>
	<content:p>
	This check allows impersonating users. If the impersonation fails, the whole request fails.
	</content:p>
	<content:plainText><![CDATA[
ObjectType = "ITEM"
ObjectID = The name of the item to write to
Operation = "WRITE"
Additional information:
	value -> The write value as Variant type]]>
	</content:plainText>
</content:section>

<content:section content:title="Write attributes to item">
	<content:p>
	Checked when requesting to write attributes to an item. 
	</content:p>
	<content:p>
	This check allows impersonating users. If the impersonation fails, the whole request fails.
	</content:p>
	<content:plainText><![CDATA[
ObjectType = "ITEM"
ObjectID = The name of the item to write to
Operation = "WRITE_ATTRIBUTES"
Additional information:
	attributes -> The write attributes as Map<String,Variant> type]]>
	</content:plainText>
</content:section>

<content:section content:title="Grant client permission">
	<content:p>
	The client may request <content:q>Client Permissions</content:q> from the server. These aid the client to remove
	content which is not intended for the user. Still it is only an informational and optional way to
	clear up the user interface, not a way to block operations.
	</content:p>
	<content:p>
	This is an ongoing evaluation. Any change will be reported to the client immediately.
	</content:p>
	<content:p>
	Rejected request will not get recorded.
	</content:p>
	<content:plainText><![CDATA[
ObjectType = "SESSION"
ObjectID = The name of the privilege to grant, as requested by the client
Operation = "PRIV"
Additional information: <none>]]>
	</content:plainText>
</content:section>

</content:section>

</content:fragment>
