This project is a short sample for using the "exec" driver. Simply
start by running:

	ant

this will download openSCADA and run the exec hive using the
configuration in configuration/exporter.xml

Press Ctrl-C to abort and use OSTC to connect.

The sample will need internet access to download openSCADA.